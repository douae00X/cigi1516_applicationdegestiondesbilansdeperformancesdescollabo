package DAO;

import entities.Feedback;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class FeedbackOperations {

       public void feedbackStorage( String collabName,String projectName,
               String dateDebut , String dateFin, String productivité ,
               String rem1, String fiabilité , String rem2 ,String tech ,
               String rem3 , String concep ,String rem4 ,String avantVente , 
               String rem5 , String gestionProjet , String rem6 ,
               String gestionRelation , String rem7, String poly ,String rem8 ,
               int ntq, int tpo, double ng, String remGlobale ) {     
     
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(""
                + "com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
        EntityManager em = emf.createEntityManager() ;
        //Ouverture
        em.getTransaction().begin() ;
  
        Feedback feedback = new Feedback() ;
        
        feedback.setCollabName(collabName);
        feedback.setProjectName(projectName);
        feedback.setDateDebut(dateDebut);
        feedback.setDateFin(dateFin);
        feedback.setProductivité(productivité);
        feedback.setRem1(rem1);
        feedback.setFiabilité(fiabilité);
        feedback.setRem2(rem2);
        feedback.setTech(tech);
        feedback.setRem3(rem3);
        feedback.setConcep(concep);
        feedback.setRem4(rem4);
        feedback.setAvantVente(avantVente);
        feedback.setRem5(rem5);
        feedback.setGestionProjet(gestionProjet);
        feedback.setRem6(rem6);
        feedback.setGestionRelation(gestionRelation);
        feedback.setRem7(rem7);
        feedback.setPoly(poly);
        feedback.setRem8(rem8);
        feedback.setNtq(ntq);
        feedback.setTpo(tpo);
        feedback.setNg(ng);
        feedback.setRemGlobale(remGlobale);
        
        String timeStamp = new SimpleDateFormat("yyyyMMdd")
                .format(Calendar.getInstance().getTime());   
        feedback.setDateFeed(timeStamp);
         
        em.persist(feedback) ;
        
        em.getTransaction().commit() ;
       
        em.close() ;     
        emf.close() ;
       
       }
       
    public Feedback getFeedByName(String collName,String dateFeed){
      Feedback feed = new Feedback() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
               
        Query query = em.createQuery(" select feed from Feedback feed where feed.collabName= :param and feed.dateFeed= :param1");//Attention = :param et non pas = : param
        query.setParameter("param", collName);
        query.setParameter("param1", dateFeed);
        feed = (Feedback) query.getSingleResult() ;
        
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return feed ;   
    }

}
