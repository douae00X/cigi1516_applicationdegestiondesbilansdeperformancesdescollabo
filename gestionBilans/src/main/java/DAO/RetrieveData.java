package DAO;

import entities.Collaborateur;
import entities.Encadrant;
import entities.Feedback;
import entities.ManagerRH;
import entities.Project;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;


public class RetrieveData {

 
   public List<ManagerRH> getRhManagers(){
     List<ManagerRH> rhManagerList = new ArrayList<ManagerRH>() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
  
        Query query = em.createQuery(" select manager from Administrateur manager ");//Attention = :param et non pas = : param
        //query.setParameter("param", "ManagerRH");
        rhManagerList = query.getResultList();
        
        for(int i=0 ;i< rhManagerList.size();i++){
            ManagerRH mrh = (ManagerRH) rhManagerList.get(i);
            System.out.println("Nom manager:"+mrh.getNom()+" Prénom manager:"+mrh.getPrenom());
        }     
 
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return rhManagerList ;   
    }
     
     public List<Encadrant> getEncadrants(){
     List<Encadrant> encList = new ArrayList<Encadrant>() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
      //System.out.println("**********************aaaahhhhhhhhhhaaaaaaaaaaaaaaaaaa************************");
      //Ouverture
        em.getTransaction().begin() ;
  
       //get all rh managers emails from database 
        Query query = em.createQuery("SELECT enc FROM Encadrant enc") ;
        encList = query.getResultList();
      
           
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return encList ;   
    }    
     
    public List<Collaborateur> getCollaborateurs(){
     List<Collaborateur> collList = new ArrayList<Collaborateur>() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
  
       //get all rh managers emails from database 
        Query query = em.createQuery("SELECT coll FROM Collaborateur coll") ;
        collList = query.getResultList();
        
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return collList ;   
    }
    
     public List<Project> getProjects(){
     List<Project> projectrList = new ArrayList<Project>() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
  
        Query query = em.createQuery(" select proj from Project proj ");//Attention = :param et non pas = : param
        //query.setParameter("param", "ManagerRH");
        projectrList = query.getResultList();
     
 
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return projectrList ;   
    }
     
    public List<Feedback> getFeedbacks(){
     List<Feedback> feedbackList = new ArrayList<Feedback>() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
  
        Query query = em.createQuery(" select feedback from Feedback feedback ");
        //query.setParameter("param", "ManagerRH");
        feedbackList = query.getResultList();
        
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return feedbackList ;   
    }
  
       
    public Collaborateur findCollaborateur(String collaborateurName){
    
      Collaborateur coll = new Collaborateur() ;
      
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
      
      String[] nomprenom = collaborateurName.split(" ") ; 
     
      //Ouverture
        em.getTransaction().begin() ;
  
        Query query = em.createQuery(" select coll from Collaborateur coll where coll.nom= :param1 and coll.prenom= :param2 ");
        query.setParameter("param1", nomprenom[0]);
        query.setParameter("param2", nomprenom[1]);
        coll = (Collaborateur) query.getSingleResult() ;
        
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return coll ;   
    }
}
