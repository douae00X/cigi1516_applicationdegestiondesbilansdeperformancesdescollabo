package DAO;

import entities.Collaborateur;
import entities.Encadrant;
import entities.ManagerRH;
import entities.Project ;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;



public class Association {
    
     public void assocPoject(String projectName,String col,Long manRh,Long encadrant) {     

                   
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
        EntityManager em = emf.createEntityManager() ;
        //Ouverture
        em.getTransaction().begin() ;
  
        //finding encadrant and manager
        ManagerRH mrh = new ManagerRH() ;
        mrh = em.find(ManagerRH.class,manRh) ;
        
        Encadrant enc = new Encadrant() ;
        enc = em.find(Encadrant.class,encadrant) ;
  
        //System.out.println("*******************Encadrant:"+enc.getNom()+" "+enc.getPrenom()) ;
        
         /*Project
         Project projet = new Project() ;
         projet.setProjectName(projectName);
         projet.setEnc(enc);
         em.persist(projet) ;
        */
        //Collaborateur persistence
        Collaborateur coll = new Collaborateur() ;
        String[] collabNomPrenom =  col.split(" ") ;

        Query query =  em.createQuery("SELECT c FROM Collaborateur c WHERE c.nom = :param1 AND c.prenom = :param2") ;
        query.setParameter("param1", collabNomPrenom[0]);
        query.setParameter("param2", collabNomPrenom[1]);
        coll = (Collaborateur) query.getSingleResult();
        if(mrh!=null)
         coll.setMrh(mrh);
        //coll.setProject(projet);
        em.persist(coll);
        
        em.getTransaction().commit() ;
        
        em.close() ;     
        emf.close() ;
       
       }
    
    
}
