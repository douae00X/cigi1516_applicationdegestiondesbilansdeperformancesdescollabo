package DAO;

import entities.Collaborateur;
import entities.Role;
import entities.Useri;
import entities.SpringRole;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Enrollement {
    
       public void ajouterCollab(Collaborateur col,Long role_id) {     

        SpringRole springUserRole = new SpringRole() ;
        Useri user = new Useri() ;
           
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
        EntityManager em = emf.createEntityManager() ;
        //Ouverture
        em.getTransaction().begin() ;
  
        //User
        user.setUsername(col.getUsername());
        user.setPassword(col.getPassword());
        user.setEnabled(true);
  
        em.persist(user) ;
        
        
        //Role
        Role role = new Role() ;
        role = em.find(Role.class, role_id);
        
        if (role == null) {
           role.setRole_id(role_id) ;
           em.persist(role) ;
        } 
        
        springUserRole.setRole(role.getRoleName()); 
           
        //
        col.setRole(role);
        em.persist(col);
        
               
        springUserRole.setUsername(col.getUsername());
        em.persist(springUserRole) ;
        
        em.getTransaction().commit() ;
       
        em.close() ;     
        emf.close() ;
       
       }
    
}
