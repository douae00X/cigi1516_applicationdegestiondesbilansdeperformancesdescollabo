package DAO;

import entities.Collaborateur;
import entities.Objectif;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;


public class ObjectifsOperations {
 
   public List<Objectif> getObjListById(String collId){
     List<Objectif> objList = new ArrayList<Objectif>() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
        
        int collabId = Integer.parseInt(collId);
               
        Query query = em.createQuery(" select obj from Objectif obj where obj.id= :param ");//Attention = :param et non pas = : param
        query.setParameter("param", collabId);
        objList = query.getResultList() ;
        
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return objList ;   
    }

    public void saveObjChanges(String objId,String note11,String note21,String note31,
            String note41,String note51,String note61,String note71,String note81){
         
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
  
        Query query = em.createQuery("UPDATE Objectif obj SET obj.notePro = :note1"
                + ", obj.noteQty= :note2 , obj.noteTech= :note3 , obj.noteConc= :note4"
                + ", obj.noteAvv= :note5 , obj.noteGp= :note6 , obj.noteGrc= :note7"
                + ",  obj.notePoly= :note8 WHERE obj.id= :objId");
	
        query.setParameter("note1", Float.parseFloat(note11));
        query.setParameter("note2", Float.parseFloat(note21));
        query.setParameter("note3", Float.parseFloat(note31));
        query.setParameter("note4", Float.parseFloat(note41));
        query.setParameter("note5", Float.parseFloat(note51));
        query.setParameter("note6", Float.parseFloat(note61));
        query.setParameter("note7", Float.parseFloat(note71));
        query.setParameter("note8", Float.parseFloat(note81));
        query.setParameter("objId", Long.parseLong(objId));
	query.executeUpdate();

             
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
            
    }

    
    public void updateObj(Objectif obj){
         
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
  
        Query query = em.createQuery("UPDATE Objectif obj SET obj.coll = :param1"
                + ", obj.dateCre= :param2 , obj.id= :param3 , obj.objAvv= :param4"
                + ", obj.objConc= :param5 , obj.objGp= :param6 , obj.objGrc= :param7"
                + ",  obj.objQty= :param8 ,obj.objTech= :param9 ,obj.objPro= :param10"
                + ", obj.objPoly= :param11 , obj.poidsAvv= :param12 ,obj.poidsConc= :param13"
                + ", obj.poidsGp= :param14 , obj.poidsGrc= :param15 , obj.poidsPoly= :param16 "
                + ", obj.poidsPro= :param17 , obj.poidsQty= :param18 , obj.poidsTech= :param19 WHERE obj.id= :objId");
        query.setParameter("param1", obj.getColl());
        query.setParameter("param2", obj.getDateCre());
        query.setParameter("param3", obj.getId());
        query.setParameter("param4", obj.getObjAvv());
        query.setParameter("param5", obj.getObjConc());
        query.setParameter("param6", obj.getObjGp());
        query.setParameter("param7", obj.getObjGrc());
        query.setParameter("param8", obj.getObjQty());
        query.setParameter("param9", obj.getObjTech());
        query.setParameter("param10", obj.getObjPro());
        query.setParameter("param11", obj.getObjPoly());
        query.setParameter("param12", obj.getPoidsAvv());
        query.setParameter("param13", obj.getPoidsConc());
        query.setParameter("param14", obj.getPoidsGp());
        query.setParameter("param15", obj.getPoidsGrc());
        query.setParameter("param16", obj.getPoidsPoly());
        query.setParameter("param17", obj.getPoidsPro());
        query.setParameter("param18", obj.getPoidsQty());
        query.setParameter("param19", obj.getPoidsTech());
        query.setParameter("objId", obj.getId());
        
          
	query.executeUpdate();

             
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
            
    }
      public void insertNewObj(Objectif obj){
         
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
  
        Objectif objectif = new Objectif() ;
        
        objectif.setColl(obj.getColl());
        objectif.setDateCre(obj.getDateCre());
        objectif.setId(obj.getId());
        objectif.setObjAvv(obj.getObjAvv());
        objectif.setObjConc(obj.getObjConc());
        objectif.setObjGp(obj.getObjGp());
        objectif.setObjGrc(obj.getObjGrc());
        objectif.setObjQty(obj.getObjQty());
        objectif.setObjTech(obj.getObjTech());
        objectif.setObjPro(obj.getObjPro());
        objectif.setObjPoly(obj.getObjPoly());
        objectif.setPoidsAvv(obj.getPoidsAvv());
        objectif.setPoidsConc(obj.getPoidsConc());
        objectif.setPoidsGp(obj.getPoidsGp());
        objectif.setPoidsGrc(obj.getPoidsGrc());
        objectif.setPoidsPoly(obj.getPoidsPoly());
        objectif.setPoidsPro(obj.getPoidsPro());
        objectif.setPoidsQty(obj.getPoidsQty());
        objectif.setPoidsTech(obj.getPoidsTech());
        
        em.persist(objectif) ;        
             
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
           
    }
      
    public Objectif getObjById(String collId,String dateCre){
      Objectif obj = new Objectif() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
        
        int collabId = Integer.parseInt(collId);
               
        Query query = em.createQuery(" select obj from Objectif obj where obj.id= :param1 and obj.dateCre= :param2 ");//Attention = :param et non pas = : param
        query.setParameter("param1", collabId);
        query.setParameter("param2", dateCre);
        obj = (Objectif) query.getSingleResult() ;
        
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return obj ;   
    }

  public Objectif getObjByName(String collName,String dateCre){
      Objectif obj = new Objectif() ;
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_gestionBilans_war_1.0-SNAPSHOTPU") ;
      EntityManager em = emf.createEntityManager() ;
     
      //Ouverture
        em.getTransaction().begin() ;
        
        String[] collNomPrenom = collName.split(" ") ;
        
        Collaborateur collaborateur = new Collaborateur() ;
                       
        Query query = em.createQuery(" select coll from Collaborateur coll where coll.nom= :param1 and coll.prenom= :param2 ");//Attention = :param et non pas = : param
        query.setParameter("param1", collNomPrenom[0]);
        query.setParameter("param2", collNomPrenom[1]);
        collaborateur = (Collaborateur) query.getSingleResult() ;
        
        obj = getObjById(""+collaborateur.getId(),dateCre) ;
        
       //commit   
        em.getTransaction().commit() ;
       
        //close entity manager
        em.close() ;     
        //close entitymanager factory
        emf.close() ;
     
     return obj ;   
    }

}
