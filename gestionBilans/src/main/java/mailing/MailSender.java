package mailing;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Transport;




@Stateless
public class MailSender {
    
    public void sendEmail(String username,String pass,String mat,String password,String sender,String receiver){
    
      try{
        Properties props = System.getProperties() ;
        props.put("mail.smtp.host","smtp.gmail.com") ;
        props.put("mail.smtp.auth","true") ;
        props.put("mail.smtp.port","465") ;
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory") ;
        props.put("mail.smtp.socketFactory.port","465") ;
        props.put("mail.smtp.socketFactory.fallback","false") ;
        
        Session mailSession = Session.getDefaultInstance(props,null) ;
        mailSession.setDebug(true) ;
        
        Message mailMessage = new MimeMessage(mailSession) ;
        mailMessage.setFrom(new InternetAddress(sender) ) ;
        mailMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(receiver)) ;
        mailMessage.setContent("Hello\nThis is your username :"+mat+"\nAnd this is your password : "+password+".","text/html") ;
        mailMessage.setSubject("Your Account!!") ;
        
          javax.mail.Transport transport = mailSession.getTransport("smtp") ;
        transport.connect("smtp.gmail.com",username,pass) ;
        
        transport.sendMessage(mailMessage , mailMessage.getAllRecipients()) ;
      
      
      }catch(Exception ex){
        Logger.getLogger(MailSender.class.getName()).log(Level.SEVERE,null,ex) ; ;
       } 
     }

    
}
