package controller;

import DAO.RetrieveData;
import entities.ManagerRH;
import java.util.ArrayList;
import java.util.List;
import mailing.RHMailSender;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


public class TestJob implements Job{

    RHMailSender rhMailSender = new RHMailSender() ;
    RetrieveData retrieveRhEmails = new RetrieveData() ;
    List<ManagerRH> rhEmailsList  = new ArrayList<ManagerRH>() ;;
    
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
       
        rhEmailsList = retrieveRhEmails.getRhManagers();
       
        for (ManagerRH rhEmailsList1 : rhEmailsList) {
            rhMailSender.sendBapEmail("haikei.pomme","pomme00X", "haikei.pomme@gmail.com", rhEmailsList1.getEmail());
        }
       
      System.out.println("Email sent to all RH managers") ;
       
    }
    
}
