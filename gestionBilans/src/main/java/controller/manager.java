package controller;

import DAO.Association;
import DAO.Enrollement;
import DAO.FeedbackOperations;
import DAO.ObjectifsOperations;
import DAO.RetrieveData;
import entities.Collaborateur;
import entities.Feedback;
import entities.ManagerRH;
import entities.Objectif;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mailing.MailSender;
import mailing.RHMailSender;


@WebServlet(name = "manager", urlPatterns = {"/manager"})
public class manager extends HttpServlet {

   @EJB
   private MailSender mailSender ;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String flag = request.getParameter("formlogin") ;

        if(flag.equals("inscription")){
        String nom = request.getParameter("nom") ;
        String prenom = request.getParameter("prenom") ;
        String password = request.getParameter("pass") ;
        String mat = request.getParameter("mat") ;
        String email =   request.getParameter("email") ;
        String dateEntree =  request.getParameter("de") ;
        
        Enrollement col = new Enrollement() ;
        Collaborateur coll = new Collaborateur() ;
        
        coll.setNom(nom);
        coll.setPrenom(prenom);
        coll.setPassword(password);
        coll.setUsername(mat);
        coll.setDateEntree(dateEntree);
              
        
        col.ajouterCollab(coll,Long.parseLong("51")) ;
        
         mailSender.sendEmail("haikei.pomme","pomme00X",mat,password,"haikei.pomme@gmail.com" ,email) ;
       
         response.sendRedirect("inscription.jsp");
        }
        else{
        if(flag.equals("feedbacksheet")){

            
            String collabName = request.getParameter("collaborateurName") ;
            String projectName = request.getParameter("projectName") ;
            String dateDebut = request.getParameter("dateDebut") ;
            String dateFin = request.getParameter("dateFin") ;
            String productivité = request.getParameter("prodQual") ;
            String rem1 = request.getParameter("r1") ;
            String fiabilité = request.getParameter("fiabQual") ;
            String rem2 = request.getParameter("r2") ;
            String tech = request.getParameter("techQual") ;
            String rem3 = request.getParameter("r3") ;
            String concep = request.getParameter("concQual") ;
            String rem4 = request.getParameter("r4") ;
            String avantVente = request.getParameter("aventeQual") ;
            String rem5 = request.getParameter("r5") ;
            String gestionProjet = request.getParameter("gestProjQual") ;
            String rem6 = request.getParameter("r6") ;
            String gestionRelation = request.getParameter("gestreclQual") ;
            String rem7 = request.getParameter("r7") ;
            String poly = request.getParameter("polyQual") ;
            String rem8 = request.getParameter("r8") ;
            String ntq = request.getParameter("NTQ") ;
            String tpo = request.getParameter("TPO") ;
            String ng = request.getParameter("NG") ;
            String remGlobale = request.getParameter("rque") ;
            
            FeedbackOperations feedbackStorage  = new FeedbackOperations();
            try{
            feedbackStorage.feedbackStorage(collabName, projectName, dateDebut, 
                    dateFin, productivité,rem1, fiabilité, rem2, tech, rem3,
                    concep, rem4, avantVente, rem5,gestionProjet, rem6, 
                    gestionRelation, rem7, poly, rem8,Integer.parseInt(ntq), 
                    Integer.parseInt(tpo), Double.parseDouble(ng), remGlobale);
            }catch(NumberFormatException e){
              System.out.println("*********This is the Error "+e+"**********") ;
            }
             
            response.sendRedirect("feedback.jsp");
            
            //Encoyer un email au manager au collaborateur et à son encadrant
             RHMailSender rhMailSender = new RHMailSender() ;
             RetrieveData retrieveRhEmails = new RetrieveData() ;
             List<ManagerRH> rhEmailsList  = new ArrayList<ManagerRH>() ;
             Collaborateur collaborateur = new Collaborateur() ;
 
             
             rhEmailsList = retrieveRhEmails.getRhManagers();
             collaborateur= retrieveRhEmails.findCollaborateur(collabName) ;
       
             for (ManagerRH rhEmailsList1 : rhEmailsList) {
                 rhMailSender.sendFeedbackEmail("haikei.pomme", "pomme00X", "haikei.pomme@gmail.com", rhEmailsList1.getEmail());
             }
                 rhMailSender.sendFeedbackEmail("haikei.pomme", "pomme00X", "haikei.pomme@gmail.com", collaborateur.getEmail());
                    
        }  
        else{
            if(flag.equals("saveObj")){
                
                
                
                response.sendRedirect("objSheet.jsp");
            
            }
            else{
                if(flag.equals("bapData")){
                                 
                 Feedback feedbackSheet = new Feedback() ;
                 Objectif objSheet = new Objectif() ;
                      
                 String collabName = request.getParameter("collName") ;
                 String dateFeed = request.getParameter("dateFeed") ;
                 String dateCre = request.getParameter("dateCre") ;
        
                 FeedbackOperations feedbackOperations = new FeedbackOperations() ;
                 ObjectifsOperations objectifsOperations = new ObjectifsOperations() ;
                 
                 feedbackSheet = feedbackOperations.getFeedByName(collabName,dateFeed) ;
                 objSheet = objectifsOperations.getObjByName(collabName, dateCre) ;
               
                 //set attributes
                 request.getSession().setAttribute("feedbackSheet", feedbackSheet);
                 request.getSession().setAttribute("objSheet", objSheet);
                        

                 response.sendRedirect("bap.jsp");
                } 
                else{
                    if(flag.equals("bipData")){
                        Objectif objSheet = new Objectif() ;
                      
                        String collabName = request.getParameter("collName") ;
                        String dateCre = request.getParameter("dateCre") ;

                        ObjectifsOperations objectifsOperations = new ObjectifsOperations() ;
             
                        objSheet = objectifsOperations.getObjByName(collabName, dateCre) ;
               
                        //set attributes
                        request.getSession().setAttribute("objSheet", objSheet);
                        

                        response.sendRedirect("bip.jsp");
                    }
                    else{    
                        if(flag.equals("sendBap")){
                            
                             String objId = request.getParameter("objId") ;
                             String note11 = request.getParameter("note11") ;
                             String note21 = request.getParameter("note21") ;
                             String note31 = request.getParameter("note31") ;
                             String note41 = request.getParameter("note41") ;
                             String note51 = request.getParameter("note51") ;
                             String note61 = request.getParameter("note61") ;
                             String note71 = request.getParameter("note71") ;
                             String note81 = request.getParameter("note81") ;
                             
                             //DAO invocation for update=> Bap
                             ObjectifsOperations objectifsOperations = new ObjectifsOperations() ;
                             objectifsOperations.saveObjChanges(objId, note11, note21, 
                               note31, note41, note51, note61, note71, note81);
                             
                             //DAO invocation for insert=> new objectifs
                             String obj12 = request.getParameter("obj12") ;
                             String poids12 = request.getParameter("poids12") ;
                             String obj22 = request.getParameter("obj22") ;
                             String poids22 = request.getParameter("poids22") ;
                             String obj32 = request.getParameter("obj32") ;
                             String poids32 = request.getParameter("poids32") ;
                             String obj42 = request.getParameter("obj42") ;
                             String poids42= request.getParameter("poids42") ;
                             String obj52 = request.getParameter("obj52") ;
                             String poids52 = request.getParameter("poids52") ;
                             String obj62 = request.getParameter("obj62") ;
                             String poids62 = request.getParameter("poids62") ;
                             String obj72 = request.getParameter("obj72") ;
                             String poids72 = request.getParameter("poids72") ;
                             String obj82= request.getParameter("obj82") ;
                             String poids82 = request.getParameter("poids82") ;
                             
                             Objectif objectif = new Objectif() ;
                             objectif.setObjPro(obj12);
                             objectif.setPoidsPro(poids12);
                             objectif.setObjQty(obj22);
                             objectif.setPoidsQty(poids22);
                             objectif.setObjConc(obj32);
                             objectif.setPoidsConc(poids32);
                             objectif.setObjTech(obj42);
                             objectif.setPoidsTech(poids42);
                             objectif.setObjAvv(obj52);
                             objectif.setPoidsAvv(poids52);
                             objectif.setObjGp(obj62);
                             objectif.setPoidsGp(poids62);
                             objectif.setObjGrc(obj72);
                             objectif.setPoidsGrc(poids72);
                             objectif.setObjPoly(obj82);
                             objectif.setPoidsPoly(poids82);
                             
                             String timeStamp = new SimpleDateFormat("yyyyMMdd")
                .format(Calendar.getInstance().getTime());  
                             objectif.setDateCre(timeStamp);
                             
                              objectifsOperations.insertNewObj(objectif);
                             
                              response.sendRedirect("bap.jsp");
                        }
                        else{
                            if(flag.equals("sendBip")){
                             
                             String obj12 = request.getParameter("obj11") ;
                             String poids12 = request.getParameter("poids11") ;
                             String obj22 = request.getParameter("obj21") ;
                             String poids22 = request.getParameter("poids21") ;
                             String obj32 = request.getParameter("obj31") ;
                             String poids32 = request.getParameter("poids31") ;
                             String obj42 = request.getParameter("obj41") ;
                             String poids42= request.getParameter("poids41") ;
                             String obj52 = request.getParameter("obj51") ;
                             String poids52 = request.getParameter("poids51") ;
                             String obj62 = request.getParameter("obj61") ;
                             String poids62 = request.getParameter("poids61") ;
                             String obj72 = request.getParameter("obj71") ;
                             String poids72 = request.getParameter("poids71") ;
                             String obj82= request.getParameter("obj81") ;
                             String poids82 = request.getParameter("poids81") ;
                             
                             Objectif objectif = new Objectif() ;
                             objectif.setObjPro(obj12);
                             objectif.setPoidsPro(poids12);
                             objectif.setObjQty(obj22);
                             objectif.setPoidsQty(poids22);
                             objectif.setObjConc(obj32);
                             objectif.setPoidsConc(poids32);
                             objectif.setObjTech(obj42);
                             objectif.setPoidsTech(poids42);
                             objectif.setObjAvv(obj52);
                             objectif.setPoidsAvv(poids52);
                             objectif.setObjGp(obj62);
                             objectif.setPoidsGp(poids62);
                             objectif.setObjGrc(obj72);
                             objectif.setPoidsGrc(poids72);
                             objectif.setObjPoly(obj82);
                             objectif.setPoidsPoly(poids82);
                             
                             //DAO invocation for update=> Bip
                              ObjectifsOperations objectifsOperations = new ObjectifsOperations() ;
                              objectifsOperations.updateObj(objectif);
                             
                              response.sendRedirect("bip.jsp");
                            
                            }
                            else{
                             String projectName = request.getParameter("project") ;
                             String collabo = request.getParameter("col") ;
                             String managerRh = request.getParameter("man") ;
                             String encad = request.getParameter("enc" );
        
                            //System.out.println("**********This encadrant id :"+encad+"***********") ;
                            Association assoc = new Association() ;
                            assoc.assocPoject(projectName,collabo,Long.parseLong(managerRh),Long.parseLong(encad)) ;
                
                             response.sendRedirect("project.jsp");
                            } 
                        } 
                    }
               }  
           }
        } 
      }   
    
        try (PrintWriter out = response.getWriter()) {
              
                    
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
   
   // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
