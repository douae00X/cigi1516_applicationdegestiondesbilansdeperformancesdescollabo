package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Feedback implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String collabName ;
    private String projectName  ;
    private String dateDebut ;
    private String dateFin  ;
    private String productivité ;
    private String rem1  ;
    private String fiabilité  ;
    private String rem2  ;
    private String tech ;
    private String rem3  ;
    private String concep  ;
    private String rem4  ;
    private String avantVente ;
    private String rem5 ;
    private String gestionProjet  ;
    private String rem6  ;
    private String gestionRelation ;
    private String rem7  ;
    private String poly ;
    private String rem8 ;
    private int ntq;
    private int tpo ;
    private double ng ;
    private String remGlobale  ;
    private String dateFeed ;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCollabName() {
        return collabName;
    }

    public void setCollabName(String collabName) {
        this.collabName = collabName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public String getProductivité() {
        return productivité;
    }

    public void setProductivité(String productivité) {
        this.productivité = productivité;
    }

    public String getRem1() {
        return rem1;
    }

    public void setRem1(String rem1) {
        this.rem1 = rem1;
    }

    public String getFiabilité() {
        return fiabilité;
    }

    public void setFiabilité(String fiabilité) {
        this.fiabilité = fiabilité;
    }

    public String getRem2() {
        return rem2;
    }

    public void setRem2(String rem2) {
        this.rem2 = rem2;
    }

    public String getTech() {
        return tech;
    }

    public void setTech(String tech) {
        this.tech = tech;
    }

    public String getRem3() {
        return rem3;
    }

    public void setRem3(String rem3) {
        this.rem3 = rem3;
    }

    public String getConcep() {
        return concep;
    }

    public void setConcep(String concep) {
        this.concep = concep;
    }

    public String getRem4() {
        return rem4;
    }

    public void setRem4(String rem4) {
        this.rem4 = rem4;
    }

    public String getAvantVente() {
        return avantVente;
    }

    public void setAvantVente(String avantVente) {
        this.avantVente = avantVente;
    }

    public String getRem5() {
        return rem5;
    }

    public void setRem5(String rem5) {
        this.rem5 = rem5;
    }

    public String getGestionProjet() {
        return gestionProjet;
    }

    public void setGestionProjet(String gestionProjet) {
        this.gestionProjet = gestionProjet;
    }

    public String getRem6() {
        return rem6;
    }

    public void setRem6(String rem6) {
        this.rem6 = rem6;
    }

    public String getGestionRelation() {
        return gestionRelation;
    }

    public void setGestionRelation(String gestionRelation) {
        this.gestionRelation = gestionRelation;
    }

    public String getRem7() {
        return rem7;
    }

    public void setRem7(String rem7) {
        this.rem7 = rem7;
    }

    public String getPoly() {
        return poly;
    }

    public void setPoly(String poly) {
        this.poly = poly;
    }

    public String getRem8() {
        return rem8;
    }

    public void setRem8(String rem8) {
        this.rem8 = rem8;
    }

    public int getNtq() {
        return ntq;
    }

    public void setNtq(int ntq) {
        this.ntq = ntq;
    }

    public int getTpo() {
        return tpo;
    }

    public void setTpo(int tpo) {
        this.tpo = tpo;
    }

    public double getNg() {
        return ng;
    }

    public void setNg(double ng) {
        this.ng = ng;
    }

    public String getRemGlobale() {
        return remGlobale;
    }

    public void setRemGlobale(String remGlobale) {
        this.remGlobale = remGlobale;
    }

    public String getDateFeed() {
        return dateFeed;
    }

    public void setDateFeed(String dateFeed) {
        this.dateFeed = dateFeed;
    }

    
}
