package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Collaborateur extends Utilisateur implements Serializable {

    private String dateEntree ;
   
    @ManyToOne
    @JoinColumn(name="role_id", insertable = false,updatable = false)
    private Role role ;
   
    @ManyToOne
    @PrimaryKeyJoinColumn(name="id")
    private Project project ; 
    
    @ManyToOne
    @JoinColumn(name="id",insertable = false,updatable = false)        
    ManagerRH mrh ;
 
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(String dateEntree) {
        this.dateEntree = dateEntree;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public ManagerRH getMrh() {
        return mrh;
    }

    public void setMrh(ManagerRH mrh) {
        this.mrh = mrh;
    }

    
}
