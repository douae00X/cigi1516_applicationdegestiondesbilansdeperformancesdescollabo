package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
public class Project implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String projectName ;
    
   /* @OneToMany(mappedBy="project")
    List<Collaborateur> collabList = new ArrayList<Collaborateur>() ;
    */
    @ManyToOne
    @JoinColumn(name="id",insertable = false,updatable = false)
    private Encadrant enc ;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id ;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

   /* public List<Collaborateur> getCollabList() {
        return collabList;
    }

    public void setCollabList(List<Collaborateur> collabList) {
        this.collabList = collabList;
    }
*/
    public Encadrant getEnc() {
        return enc;
    }

    public void setEnc(Encadrant enc) {
        this.enc = enc;
    }
      
    
}
