package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long role_id;
    private String roleName ;
   
    @OneToMany
    private Set<Collaborateur> users_list = new HashSet<Collaborateur>() ;
    @OneToMany
    private Set<Administrateur> admin_list = new HashSet<Administrateur>() ;
     

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<Collaborateur> getUsers_list() {
        return users_list;
    }

    public void setUsers_list(Set<Collaborateur> users_list) {
        this.users_list = users_list;
    }

    public Set<Administrateur> getAdmin_list() {
        return admin_list;
    }

    public void setAdmin_list(Set<Administrateur> admin_list) {
        this.admin_list = admin_list;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (role_id != null ? role_id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role)) {
            return false;
        }
        Role other = (Role) object;
        if ((this.role_id == null && other.role_id != null) || (this.role_id != null && !this.role_id.equals(other.role_id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Role[ id=" + role_id + " ]";
    }
    
}
