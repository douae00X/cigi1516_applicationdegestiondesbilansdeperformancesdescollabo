package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Encadrant extends Utilisateur implements Serializable {

    @OneToMany(mappedBy="enc")
    private List<Project> projectList = new ArrayList<Project>() ;

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }
    
    
}
