package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Objectif implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String objPro ;
    private String poidsPro ;
    private float notePro ;
    private String objQty ;
    private String poidsQty ;
    private float noteQty ;
    private String objTech ;
    private String poidsTech ;
    private float noteTech ;
    private String objConc ;
    private String poidsConc ;
    private float noteConc ;
    private String objAvv ;
    private String poidsAvv ;
    private float noteAvv ;
    private String objGp ;
    private String poidsGp ;
    private float noteGp ;
    private String objGrc ;
    private String poidsGrc ;
    private float noteGrc ;
    private String objPoly ;
    private String poidsPoly ;
    private float notePoly ;
    private String dateCre ;

    
    
    @ManyToOne
    @JoinColumn(name="id", insertable = false,updatable = false)
    private Collaborateur coll ;
   

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getObjPro() {
        return objPro;
    }

    public void setObjPro(String objPro) {
        this.objPro = objPro;
    }

    public String getPoidsPro() {
        return poidsPro;
    }

    public void setPoidsPro(String poidsPro) {
        this.poidsPro = poidsPro;
    }

    public float getNotePro() {
        return notePro;
    }

    public void setNotePro(float notePro) {
        this.notePro = notePro;
    }

    public String getObjQty() {
        return objQty;
    }

    public void setObjQty(String objQty) {
        this.objQty = objQty;
    }

    public String getPoidsQty() {
        return poidsQty;
    }

    public void setPoidsQty(String poidsQty) {
        this.poidsQty = poidsQty;
    }

    public float getNoteQty() {
        return noteQty;
    }

    public void setNoteQty(float noteQty) {
        this.noteQty = noteQty;
    }

    public String getObjTech() {
        return objTech;
    }

    public void setObjTech(String objTech) {
        this.objTech = objTech;
    }

    public String getPoidsTech() {
        return poidsTech;
    }

    public void setPoidsTech(String poidsTech) {
        this.poidsTech = poidsTech;
    }

    public float getNoteTech() {
        return noteTech;
    }

    public void setNoteTech(float noteTech) {
        this.noteTech = noteTech;
    }

    public String getObjConc() {
        return objConc;
    }

    public void setObjConc(String objConc) {
        this.objConc = objConc;
    }

    public String getPoidsConc() {
        return poidsConc;
    }

    public void setPoidsConc(String poidsConc) {
        this.poidsConc = poidsConc;
    }

    public float getNoteConc() {
        return noteConc;
    }

    public void setNoteCon(float noteConc) {
        this.noteConc = noteConc;
    }

    public String getObjAvv() {
        return objAvv;
    }

    public void setObjAvv(String objAvv) {
        this.objAvv = objAvv;
    }

    public String getPoidsAvv() {
        return poidsAvv;
    }

    public void setPoidsAvv(String poidsAvv) {
        this.poidsAvv = poidsAvv;
    }

    public float getNoteAvv() {
        return noteAvv;
    }

    public void setNoteAvv(float noteAvv) {
        this.noteAvv = noteAvv;
    }

    public String getObjGp() {
        return objGp;
    }

    public void setObjGp(String objGp) {
        this.objGp = objGp;
    }

    public String getPoidsGp() {
        return poidsGp;
    }

    public void setPoidsGp(String poidsGp) {
        this.poidsGp = poidsGp;
    }

    public float getNoteGp() {
        return noteGp;
    }

    public void setNoteGp(float noteGp) {
        this.noteGp = noteGp;
    }

    public String getObjGrc() {
        return objGrc;
    }

    public void setObjGrc(String objGrc) {
        this.objGrc = objGrc;
    }

    public String getPoidsGrc() {
        return poidsGrc;
    }

    public void setPoidsGrc(String poidsGrc) {
        this.poidsGrc = poidsGrc;
    }

    public float getNoteGrc() {
        return noteGrc;
    }

    public void setNoteGrc(float noteGrc) {
        this.noteGrc = noteGrc;
    }

    public String getObjPoly() {
        return objPoly;
    }

    public void setObjPoly(String objPoly) {
        this.objPoly = objPoly;
    }

    public String getPoidsPoly() {
        return poidsPoly;
    }

    public void setPoidsPoly(String poidsPoly) {
        this.poidsPoly = poidsPoly;
    }

    public float getNotePoly() {
        return notePoly;
    }

    public void setNotePoly(float notePoly) {
        this.notePoly = notePoly;
    }

    public String getDateCre() {
        return dateCre;
    }

    public void setDateCre(String dateCre) {
        this.dateCre = dateCre;
    }

  
    public Collaborateur getColl() {
        return coll;
    }

    public void setColl(Collaborateur coll) {
        this.coll = coll;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objectif)) {
            return false;
        }
        Objectif other = (Objectif) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Objectifs[ id=" + id + " ]";
    }
    
}
