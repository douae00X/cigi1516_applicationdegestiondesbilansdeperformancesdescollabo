<%@page import="entities.Encadrant"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<%@page import="entities.ManagerRH"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="DAO.RetrieveData"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Jumbotron tutorial from BootstrapBay.com">
    <meta name="author" content="BootstrapBay.com">
    <title>Project Association</title>
    <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css/custom.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    

  </head>
  <%!
    List<Collaborateur> collList = new ArrayList<Collaborateur>() ;
    List<ManagerRH> rhManagerList = new ArrayList<ManagerRH>() ;
    List<Encadrant> encList = new ArrayList<Encadrant>() ;
    int i ;

  %>
  <%
    RetrieveData retrieveData = new RetrieveData() ;
    collList      = retrieveData.getCollaborateurs() ;
    rhManagerList = retrieveData.getRhManagers() ;
    encList       = retrieveData.getEncadrants() ;
 
  
  %>
  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Welcome this is your Dashboard </a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="admin.jsp">Home</a></li>       
            <li><a href="inscription.jsp">Enroll Collaborator</a></li> 
            <li class="active"><a href="#">Associate Project</a></li> 
                <c:url value="/j_spring_security_logout" var="logoutUrl" /> 
            <li><h2><a href="${logoutUrl}" class="btn btn-primary">Logout</a></h2></li>  
          </ul>
         
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
	<div class="container">     
    	    <div class="modal-dialog">
    		<div class="modal-content">
	 		<form class="form-horizontal" role="form" method="post" action="manager">
    			    <div class="modal-header">
    			       <h4>Project Association</h4>
	    		    </div>
	    		    <div class="modal-body">
                                                
    				<div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Intitulé du projet</label>
    					 <div class="col-sm-10">
    					     <input type="text" class="form-control" name="project" placeholder="Quel est le titre du projet?">
    					 </div>
    			        </div>
                                
                                <div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Collaborateur</label>
    					 <div class="col-sm-10">
    					     <select  class="form-control" name="col">
                                                     <%for(i=0 ; i< collList.size() ; i++) {%>
                                                 <option><%=collList.get(i).getNom()+" "+collList.get(i).getPrenom()%></option>
                                                <%}%>                                               
                                             </select>    
    					 </div>
    			        </div>
                                
                                <div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Manager</label>
    					 <div class="col-sm-10">
    					     <select  class="form-control" name="man">
                                                    <%for(i=0 ; i< rhManagerList.size() ; i++) {%>
                                                 <option value="<%=i+1%>"><%=rhManagerList.get(i).getNom()+" "+encList.get(i).getPrenom()%></option>
                                                <%}%>  
                                             </select>    
    					 </div>
    			        </div>
                                
                                <div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Encadrant</label>
    					 <div class="col-sm-10">
    					     <select  class="form-control" name="enc">
                                                <%for(i=0 ; i< encList.size() ; i++) {%>
                                                 <option value="<%=i+1%>"><%=encList.get(i).getNom()+" "+encList.get(i).getPrenom()%></option>
                                                <%}%>                                            
                                             </select>    
    					 </div>
    			        </div>
                            
						
	    		    </div>
	    		    <div class="modal-footer">
    				<button type="submit" class="btn btn-primary">Associer</button>
    			    </div>
                                <input type="hidden" name="formlogin" value="association"/>
    			</form>
                </div>
            </div>   
	</div>	
		    
    <!-- Fixed footer -->
    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
    	<div class="container">

    	</div>
    </div>

    
    <!--===================== JavaScript===================== -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>