<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
 <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="author" content="BootstrapBay.com">
              <title>Login Form </title>  
      <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
      <link href="bootstrap-3.3.6-dist/css/custom.css" rel="stylesheet">
      <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    </head>	
	<body>

    	<div class="modal-dialog">
    		<div class="modal-content">
                    <c:url value="/j_spring_security_check" var="loginUrl" />
    			<form class="form-horizontal" role="form" method="post" action="${loginUrl}">
    			    <div class="modal-header">
    			       <h4>Login Here<h4>
	    		    </div>
	    		    <div class="modal-body">
                                    
                                     <c:if test="${SPRING_SECURITY_LAST_EXCEPTION != null}">
                                     <table>
                                      <tr>  
                                        <td colspan="2" style="color: red"> 
                                        <h3><c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" /></h3>
                                        </td>  
                                      </tr>  
                                     </table>
                                     </c:if>
            
    				<div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Matricule:</label>
    					 <div class="col-sm-10">
    					     <input type="text" class="form-control" id="contact-name" name="username" placeholder="Matricule">
    					 </div>
    			        </div>
    				<div class="form-group">
    				     <label for="pass" class="col-sm-2 control-label">Password</label>
    					<div class="col-sm-10">
    					      <input type="password" class="form-control" id="pass" name="password" placeholder="Password">
    					</div>
    				</div>
	    		    </div>
	    		    <div class="modal-footer">
    				<button type="submit" class="btn btn-primary">Login</button>
    			    </div>
    			</form>
    		</div>
    	</div>
    
	    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
  </body>
  
</html>	