<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Jumbotron tutorial from BootstrapBay.com">
    <meta name="author" content="BootstrapBay.com">
    <title>Enrollement</title>
    <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css/custom.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    

  </head>
  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Welcome this is your Dashboard </a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="admin.jsp">Home</a></li>       
            <li class="active"><a href="#">Enroll Collaborator</a></li>
            <li><a href="project.jsp">Associate Project</a></li> 
            <li><a href="#">Notifications</a></li>
                <c:url value="/j_spring_security_logout" var="logoutUrl" /> 
            <li><h2><a href="${logoutUrl}" class="btn btn-primary">Logout</a></h2></li>  
          </ul>
         
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
	<div class="container">     
    	    <div class="modal-dialog">
    		<div class="modal-content">
	 		<form class="form-horizontal" role="form" method="post" action="manager">
    			    <div class="modal-header">
    			       <h4>Enrollement</h4>
	    		    </div>
	    		    <div class="modal-body">
                                                
    				<div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Nom:</label>
    					 <div class="col-sm-10">
    					     <input type="text" class="form-control" name="nom" placeholder="Nom">
    					 </div>
    			        </div>
                                <div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Prénom:</label>
    					 <div class="col-sm-10">
    					     <input type="text" class="form-control" name="prenom" placeholder="Prénom">
    					 </div>
    			        </div>
				    <div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Matricule:</label>
    					 <div class="col-sm-10">
    					     <input type="text" class="form-control" name="mat" placeholder="Matricule">
    					 </div>
    			        </div>
    				<div class="form-group">
    				     <label for="pass" class="col-sm-2 control-label">Mot de Passe</label>
    					<div class="col-sm-10">
    					      <input type="password" class="form-control" name="pass" placeholder="Mot de¨passe">
    					</div>
    				</div>
                                <div class="form-group">
    				     <label for="email" class="col-sm-2 control-label">Adresse Email</label>
    					<div class="col-sm-10">
    					      <input type="text" class="form-control" name="email" placeholder="Adresse Email">
    					</div>
    				</div>
				
				<div class="form-group">
    				     <label for="contact-name" class="col-sm-2 control-label">Date début:</label>
    					 <div class="col-sm-10">
    					     <input type="date" class="form-control" name="de" >
    					 </div>
    			        </div>
	    		    </div>
	    		    <div class="modal-footer">
    				<button type="submit" class="btn btn-primary">Inscrire</button>
    			    </div>
                            <input type="hidden" name="formlogin" value="inscription"/>
    			</form>
                </div>
            </div>   
	</div>	
		    
    <!-- Fixed footer -->
    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
    	<div class="container">

    	</div>
    </div>

    
    <!--===================== JavaScript===================== -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>