<%@page import="DAO.ObjectifsOperations"%>
<%@page import="entities.Objectif"%>
<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 

<html>
<head>
<meta charset="utf-8">
<link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
<link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
<link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
<link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
<link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
<link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfonts-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700|Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--//webfonts-->

</head>
 
 
     <%!
    List<Collaborateur> collList = new ArrayList<Collaborateur>() ;
    List<Feedback> feedList = new ArrayList<Feedback>() ;
    List<Objectif> objList = new ArrayList<Objectif>() ;
    RetrieveData retrieveData = new RetrieveData() ;
    ObjectifsOperations objectifsOperations = new ObjectifsOperations() ;
    int i ;
  %>
  <%
   
    collList      = retrieveData.getCollaborateurs() ;  
    feedList      = retrieveData.getFeedbacks();  
    //collObjList      = objectifsOperations.getObjList(collId) ; 
  %> 
  
<body>
       
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                   <div class="container-fluid">      
     <fieldset>
      <legend>Calcul de BIP</legend>
      <div class="row-fluid">
        <div class="span8">
          <div class="row-fluid">
            <div class="span12 bgcolor">
             
            </div>
          </div> 
          <form action="manager" method="post">
           <div class="row-fluid">
            <div>
             <div class="control-group">
               <label class="control-label" for="project">Collaborateur</label>
                <div class="controls">
                    <select name="collName" class="span12">
                         <option></option>
                            <%for(i=0 ; i< collList.size() ; i++) {%>
                            <option><%=collList.get(i).getNom()%>
                              <%=collList.get(i).getPrenom()%>
                            </option>
                      <%}%> 
                    </select>  
                </div>
              </div>
            </div><!--/span-->
          </div><!--/row-->   
          <div class="row-fluid">
            <div class="span6 bgcolor">
              <div class="control-group">
                <label class="control-label" for="coll">Date de rédaction des Objectifs</label>
                <div class="controls">
                    <input type="date" name="dateCre" />  
                </div>
              </div>
            </div><!--/span--> 
          </div>   
          <div class="row-fluid">
            <div class="span6 bgcolor">
              <div class="control-group">
                <div class="controls">
                   <input type="hidden" name="formlogin" value="bipData"/>      
                   <input class="btn btn-primary" onclick='javascript:window.resizeTo(screen.width,screen.height);' type="submit" value="Deliver" name="Bapdatabutton"/>   
                </div>
              </div>
            </div><!--/span--> 
          </div>   
         </form> 
        </div>
      </div>              
                    
    </div>
   </div>
  </div>
 </div>

</div>
   	 
</body>
</html>