<%@page import="DAO.ObjectifsOperations"%>
<%@page import="entities.Objectif"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
 <head>
 
     <meta charset="utf-8">
    
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
     progress {
        color: #0063a6;
        font-size: .6em;
        line-height: 1.5em;
        text-indent: .5em;
        width: 92.5em;
        height: 3em;
        border: 1px solid #0063a6;
        background: #fff;
     }
    </style>
    
    <title>New Feedback</title>
  </head>
 
     <%! ;
    List<Objectif> objList ;
    String indice ;
    int i ;
  %>
  <%
   objList = (ArrayList<Objectif>) request.getSession().getAttribute("collObjList") ;
   indice = request.getParameter("indice") ;
   i = Integer.parseInt(indice) ;
    
  %>
           
 
  <body>
      
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
    
        </div>
      </div>
    </div>
      
     <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                  <img src="bootstrap-3.3.6-dist/img/user.png" height="42" width="42"/>
                   <font size="3" color="#fff">greenday</font>
                </li>
                <li>
                    <a href="admin.jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-home"></i>&nbsp;Home</font></a>
                </li>
                 <li>
                    <a href="#" onclick='javascript:window.open("popup.jsp", "_blank", "scrollbars=1,resizable=1,height=500,width=850");'>
                        <font size="3" color="#fff"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;New BAP</font>
                    </a>
                </li>
                <li>
                    <a href="bip.jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;New BIP</font></a>
                </li>
                <li>
                    <a href=".jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;Collaborators performance</font></a>
                </li>
            </ul>
        </div>
      
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <form action="manager" method="post">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                         

    
    <div class="container-fluid">      
          <fieldset>
        
 
             <div class="container">
    <div ng-app="myApp" ng-controller="myCtrl"> 
      <table style="position: absolute; top: 45px; left: 30px" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Thème</th>
            <th>Objectifs</th>
            <th>Poids</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Productivité</td>
            <td>
                <input type="text" style="color:#0063a6" value="<%= objList.get(i).getObjPro()%>" name="obj11" />
            </td>
            <td>
                <input type="text" style="color:green" value="<%= objList.get(i).getPoidsPro() %>" name="poids11" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= objList.get(i).getNotePro()%>" name="note11" />
            </td>
          </tr>
          <tr>
            <td>Qualité/Fiabilité</td>
            <td>
                <input type="text" style="color:#0063a6" value="<%= objList.get(i).getObjQty()%>" name="obj21" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= objList.get(i).getPoidsQty() %>" name="poids21" />   
            </td>
            <td>
                <input type="text" style="color:red"value="<%= objList.get(i).getNoteQty()%>" name="note21" />
            </td>
           </tr>
          <tr>
            <td>Technicité</td>
            <td>
                <input type="text" style="color:#0063a6" value="<%=objList.get(i).getObjTech()%>" name="obj31" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%=objList.get(i).getPoidsTech()%>" name="poids31" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%=objList.get(i).getNoteTech()%>" name="note31" />
            </td>
          </tr>
          <tr>
            <td>Conception</td>
            <td>
               <input type="text" style="color:#0063a6" value="<%= objList.get(i).getObjConc()%>" name="obj41" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= objList.get(i).getPoidsConc()%>" name="poids41" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= objList.get(i).getNoteConc()%>" name="note41" />
            </td>
          </tr>
          <tr>
            <td>Avant-vente</td>
            <td>
                <input type="text" style="color:#0063a6" value="<%= objList.get(i).getObjAvv()%>" name="obj51" /> 
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= objList.get(i).getPoidsAvv()%>" name="poids51" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= objList.get(i).getNoteAvv()%>" name="note51" />
            </td>
          </tr>
          <tr>
            <td>Gestion de projet</td>
            <td>
                <input type="text" style="color:#0063a6" value="<%= objList.get(i).getObjGp()%>" name="obj61" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= objList.get(i).getPoidsGp()%>" name="poids61" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= objList.get(i).getNoteGp()%>" name="note61" />
            </td>
          </tr>
	  <tr>
            <td>Gestion de relation client</td>
            <td>
                <input type="text" style="color:#0063a6" value="<%= objList.get(i).getObjGrc()%>" name="obj71" />
            </td>
            <td>
                <input type="text" style="color:green" value="<%= objList.get(i).getPoidsGrc()%>" name="poids71" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= objList.get(i).getNoteGrc()%>" name="note71" />
            </td>
          </tr>
          <tr>
            <td>Polyvalence</td>
            <td>
                <input type="text" style="color:#0063a6" value="<%= objList.get(i).getObjPoly()%>" name="obj81" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= objList.get(i).getPoidsPoly()%>" name="poids81" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= objList.get(i).getNotePoly()%>" name="note81" />
            </td>
          </tr>
          <tr>
              <td colspan="2">          
              </td>
              <td>
              </td>  
                 
           </tr>      
       
        </tbody>
      </table>
    </div>              
                   
          </fieldset>                        
        </div><!--/span-->
        
        <div class="span4">
          <table border="0"  style="position: absolute; top: 500px; left: 950px">
        <tr>
            <td></td>
            <td></td>
            <td>
              <input class="btn btn-primary" type="submit" value="Save changes" name="obj"/>
            </td>    
                
        </tr>    
      </table>    
        </div><!--/span-->       
      </div><!--/row-->  
              <input type="hidden" value="saveObj" name="formlogin"/>                
        </div>
      </div>
     </form>         
    </div>
   </div>


</body>
</html>
