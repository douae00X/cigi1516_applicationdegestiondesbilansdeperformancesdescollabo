<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Jumbotron tutorial from BootstrapBay.com">
    <meta name="author" content="BootstrapBay.com">
    <title>Dashboard</title>
    <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css/custom.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    

  </head>
  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <c:url value="/j_spring_security_logout" var="logoutUrl" />  
          <h3><a href="${logoutUrl}" class="btn btn-primary">Logout</a></h3> 
          <a class="navbar-brand" href="#">Welcome this is your Dashboard </a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#">Home</a></li>           
            <li><a href="userFeedback.jsp">Feedback</a></li>
            <li><a href="userObj.jsp">Objectifs Sheets</a></li> 
             
          </ul>
         
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
	<div class="container">
		<div class="jumbotron text-center">
            <c:url value="/j_spring_security_logout" var="logoutUrl" />  
            <center>  
            <h2>User | You are now logged in</h2>  
            </center>  
		</div>
	</div>	
		    
    <!-- Fixed footer -->
    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
    	<div class="container">

    	</div>
    </div>

    
    <!-- =====================JavaScript======================= -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>