<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
        
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
    </style>
    
        
    <title>New Feedback</title>
    
  </head>
  
   
      <%!
    List<Collaborateur> collList = new ArrayList<Collaborateur>() ;
    int i ;
  %>
  <%
    RetrieveData retrieveData = new RetrieveData() ;
    collList      = retrieveData.getCollaborateurs();  
  %>
           
  
  <body>
  <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
              <c:url value="/j_spring_security_logout" var="logoutUrl" /> 
          <h3><a href="${logoutUrl}" class="btn btn-primary">Logout</a></h3>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#">Home</a></li>       
            <li><a href="inscription.jsp">Enroll Collaborator</a></li>   
            <li><a href="project.jsp">Associate Project</a></li>
            <li><a href="feedbackWall.jsp">Feedback</a></li> 
          </ul>
         
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
   
     <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                  <img src="bootstrap-3.3.6-dist/img/user.png" height="42" width="42"/>
                  <font size="3" color="#fff">greenday</font>
                </li>
                <li>
                    <a href="#" onclick='javascript:window.open("popup.jsp", "_blank", "scrollbars=1,resizable=1,height=500,width=850");'>
                        <font size="3" color="#fff"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;New BAP</font>
                    </a>
                </li>
                <li>
                     <a href="#" onclick='javascript:window.open("popup1.jsp", "_blank", "scrollbars=1,resizable=1,height=500,width=850");'>
                        <font size="3" color="#fff"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;New BIP</font>
                    </a>
                </li>
                <li>
                    <a href="collPerf.jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;Collaborators performance</font></a>
                </li>
            </ul>
        </div>
      
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                    <table class="table table-bordered table-striped">
                      
                       <%for(i=0 ; i< collList.size() ; i++) {%>
                         <tr>
                           <td>  
                               <a href="objSheets.jsp?collabId=<%=collList.get(i).getId()%>" >
                              Fiches d'objectifs du collaborateur <%=collList.get(i).getNom()%>&nbsp;&nbsp;<%=collList.get(i).getPrenom()%>

                          </a> 
                          </td>
                         </tr> 
                       <%}%> 
                    </table>  
                  </div>
                </div>
            </div>
 
        </div>

 </div>
        
</body>
</html>
