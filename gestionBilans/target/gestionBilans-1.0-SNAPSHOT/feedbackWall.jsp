<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
    <!------------------------------------------------------------------------------------->
    <link href="usmanhalalit-charisma-bf07c0e/css/charisma-app.css" rel="stylesheet">
    <link href='usmanhalalit-charisma-bf07c0e/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='usmanhalalit-charisma-bf07c0e/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/css/jquery.noty.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/css/noty_theme_default.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/css/elfinder.min.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/css/elfinder.theme.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/css/uploadify.css' rel='stylesheet'>
    <link href='usmanhalalit-charisma-bf07c0e/css/animate.min.css' rel='stylesheet'>
    
        
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
    </style>
    
        
    <title>New Feedback</title>
    
  </head>
  
   
      <%!
    List<Feedback> feedList = new ArrayList<Feedback>() ;
    int i ;
  %>
  <%
    RetrieveData retrieveData = new RetrieveData() ;
    feedList      = retrieveData.getFeedbacks();  
  %>
           
  
  <body>
  
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
    
        </div>
      </div>
    </div>
     <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                 
                </li>
                <li>
                    <a href="admin.jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-home"></i>&nbsp;Home</font></a>
                </li>
            </ul>
        </div>
      
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                     <table class="table table-bordered table-striped"> 
                        <tr> 
                         <td>
                          <a href="feedbackSheet.jsp?indice=<%= feedList.size()-1%>"><b>Feedback du collaborateur <%=feedList.get(feedList.size()-1).getCollabName()%></b></a><br>
                         </td>
                        </tr>  
                          <%for(i=0 ; i< feedList.size()-1 ; i++) {%>
                          <tr>
                            <td>  
                          <a href="feedbackSheet.jsp?indice=<%=i%>" >
                              Feedback du collaborateur <%=feedList.get(i).getCollabName()%>
                          </a> 
                         </td>
                        </tr>  
                       <%}%>
                       
                     </table>  
                  </div>
                </div>
            </div>
 
        </div>

 </div>
        
</body>
</html>
