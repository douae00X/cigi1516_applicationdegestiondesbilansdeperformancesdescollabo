<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
  <head>
    <meta charset="utf-8">
    
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
     progress {
        color: #0063a6;
        font-size: .6em;
        line-height: 1.5em;
        text-indent: .5em;
        width: 92.5em;
        height: 3em;
        border: 1px solid #0063a6;
        background: #fff;
     }
    </style>
    
    <title>New Feedback</title>
    
  </head>
  
      <%!
    List<Feedback> feedList = new ArrayList<Feedback>() ;
    String indice ;
    int i ;
  %>
  <%
    RetrieveData retrieveData = new RetrieveData() ;
    feedList      = retrieveData.getFeedbacks();  
    indice = request.getParameter("indice") ;
    i = Integer.parseInt(indice) ;
  %>
  <body>
      
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
    
        </div>
      </div>
    </div>
      
     <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                 <li class="sidebar-brand">
                 
                </li>
                <li>
                    <a href="admin.jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-home"></i>&nbsp;Home</font></a>
                </li>
                <li>
                    <a href="feedbackWall.jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-align-justify"></i>&nbsp;Historique</font></a>
                </li>
            </ul>
        </div>
      
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                         

    
    <div class="container-fluid">      
      <form class="form-horizontal">
      <legend>Entête</legend>
      <div class="row-fluid">
        <div class="span8">
          <div class="row-fluid">
            <div class="span12 bgcolor">
             
            </div>
          </div> 
          <div class="row-fluid">
            <div>
             <div class="control-group">
               <label class="control-label" for="project">Projet</label>
                <div class="controls">
                    <input type="text" name="projet" value="<%= feedList.get(i).getProjectName()%>" readonly/>
                </div>
              </div>
            </div><!--/span-->
          </div><!--/row-->  
          <div class="row-fluid">
            <div class="span6 bgcolor">
              <div class="control-group">
                <label class="control-label" for="coll">Collaborateur</label>
                <div class="controls">
                   <input type="text" name="collaborateurName" value="<%= feedList.get(i).getCollabName()%>" readonly/>  
                </div>
              </div>
            </div><!--/span-->
          <div class="row-fluid">
         <div class="span6 bgcolor">
              <div class="control-group">
                 <label class="control-label" for="dateDebut" >Date début</label>
                <div class="controls">
                  <input type="text" name="dateDebut" value="<%= feedList.get(i).getDateDebut()%>" readonly>
                </div>
              </div>
            </div><!--/span-->
            <div class="span6 bgcolor">
              <div class="control-group">
                 <label class="control-label" for="dateFin">Date fin</label>
                <div class="controls">
                  <input type="text" name="dateFin" value="<%= feedList.get(i).getDateFin()%>" readonly>
                </div>
              </div>
            </div><!--/span-->
          </div><!--/row-->
          
          <fieldset>
          <legend>Détails</legend>
             <div id="wrapper">
 
             <div class="container">
   
      <table style="position: absolute; top: 270px; left: 30px" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Thème</th>
            <th>Qualification *</th>
            <th>Remarque</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Productivité</td>
            <td>
               <input type="text" name="prod" value="<%= feedList.get(i).getProductivité()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r1" value="<%= feedList.get(i).getRem1()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Qualité/Fiabilité</td>
            <td>
                 <input type="text" name="qty" value="<%= feedList.get(i).getFiabilité()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r2" value="<%= feedList.get(i).getRem2()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Technicité</td>
            <td>
                <input type="text" name="tech" value="<%= feedList.get(i).getTech()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r3" value="<%= feedList.get(i).getRem3()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Conception</td>
            <td>
                <input type="text" name="conc" value="<%= feedList.get(i).getConcep()%>" readonly/>   
            </td>
            <td>
                <input type="text" name="r4" value="<%= feedList.get(i).getRem4()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Avant-vente</td>
            <td>
                <input type="text" name="avte" value="<%= feedList.get(i).getAvantVente()%>" readonly/>   
            </td>
            <td>
                <input type="text" name="r5" value="<%= feedList.get(i).getRem5()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Gestion de projet</td>
            <td>
                <input type="text" name="gp" value="<%= feedList.get(i).getGestionProjet()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r6" value="<%= feedList.get(i).getRem6()%>" readonly/>
            </td>
          </tr>
	  <tr>
            <td>Gestion de relation client</td>
            <td>
               <input type="text" name="grc" value="<%= feedList.get(i).getGestionRelation()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r7" value="<%= feedList.get(i).getRem7()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Polyvalence</td>
            <td>
               <input type="text" name="poly" value="<%= feedList.get(i).getPoly()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r8" value="<%= feedList.get(i).getRem8()%>" readonly/>
            </td>
          </tr>
          <tr>
              <td>
                <ul type="disc">  
                <li> <font color="#4097EE">Nombre de thèmes qualifiés</font></li>
                <li> <font color="#4097EE">Total poids obtenu</font></li>
                <li> <font color="#4097EE">Nombre global</font></li>
               </ul>
              </td>
              <td colspan="2">
                  <input type="text" value="<%= feedList.get(i).getNtq()%>" name="NTQ" readonly/><br>
                  <input type="text" value="<%= feedList.get(i).getTpo()%>" name="TPO" readonly/><br>
                  <input type="text" value="<%= feedList.get(i).getNg()%>" name="NG" readonly/>
              </td>    
             
          </tr>
          <tr>
              <td colspan="3">
                  <progress id="progressbar" class="progressbar" value="40" max="100"></progress>
              </td>    
             
          </tr>
          <tr>
              <td><b>Remarque générale *:</b></td>
              <td colspan="2">
                  <input  name="rque" style="width:590px;height:200px" value="<%= feedList.get(i).getRemGlobale()%>" readonly/>
              </td>                 
          </tr>         
        </tbody>
      </table>
    </div>              
                   
          </fieldset>                        
        </div><!--/span-->
        
        <div class="span4">
          <table border="0"  style="position: absolute; top: 1040px; left: 950px">
        <tr>
            <td></td>
            <td></td>
            <td>
              <input class="btn btn-primary" type="submit" value="Post feedback" name="feedback"/>
            </td>    
                
        </tr>    
      </table>    
        </div><!--/span-->       
      </div><!--/row-->  

     </form>

                                  
        </div>
      </div>
     </div>
   </div>
 
  </div>

 </div>
        </div>
</body>
</html>
