<%@page import="DAO.ObjectifsOperations"%>
<%@page import="entities.Objectif"%>
<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
    
    <!-- Bootstrap Core CSS -->
    <link href="charts/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="charts/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="charts/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="charts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
        
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
    </style>
    
        
    <title>New Feedback</title>
    
  </head>
  
     <%!
    List<Collaborateur> collList = new ArrayList<Collaborateur>() ;
    String indice ;
    int i ;
  %>
  <%
   indice = request.getParameter("indice") ;
   i = Integer.parseInt(indice) ;
   collList      =  (List<Collaborateur>) request.getSession().getAttribute("collList") ;
  %>      
  
  <body>
  
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
    
        </div>
      </div>
    </div>
     <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                  <img src="bootstrap-3.3.6-dist/img/user.png" height="42" width="42"/>
                </li>
                <li>
                    <a href="admin.jsp"><font size="3" color="#fff">Home</font></a>
                </li>
                 <li>
                      <a href="#" onclick='javascript:window.open("popup.jsp", "_blank", "scrollbars=1,resizable=1,height=500,width=850");'>
                        <font size="3" color="#fff">New BAP</font>
                    </a>
                </li>
                <li>
                    <a href="#" onclick='javascript:window.open("popup1.jsp", "_blank", "scrollbars=1,resizable=1,height=500,width=850");'>
                        <font size="3" color="#fff">New BIP</font>
                    </a>
                </li>
                <li>
                    <a href=".jsp"><font size="3" color="#fff">Collaborators performance</font></a>
                </li>
            </ul>
        </div>
      
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                    <h3><font color="#">Performances de <%= collList.get(i).getNom()+" "+collList.get(i).getPrenom()%></font></h3>  
                  </div>
                  
                  
                      <!-- Flot Charts -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Flot Charts</h2>
                        <p class="lead">Flot is a pure JavaScript plotting library for jQuery, with a focus on simple usage, attractive looks and interactive features. For full usage instructions and documentation for Flot Charts, visit <a href="http://www.flotcharts.org/">http://www.flotcharts.org/</a>.</p>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Line Graph Example with Tooltips</h3>
                            </div>
                            <div class="panel-body">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-line-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Pie Chart Example with Tooltips</h3>
                            </div>
                            <div class="panel-body">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-pie-chart"></div>
                                </div>
                                <div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Multiple Axes Line Graph Example with Tooltips and Raw Data</h3>
                            </div>
                            <div class="panel-body">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-multiple-axes-chart"></div>
                                </div>
                                <div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Moving Line Chart</h3>
                            </div>
                            <div class="panel-body">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-moving-line-chart"></div>
                                </div>
                                <div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Bar Graph with Tooltips</h3>
                            </div>
                            <div class="panel-body">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-bar-chart"></div>
                                </div>
                                <div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                
                
                </div>
            </div>
 
        </div>

 </div>
    <!-- jQuery -->
    <script src="charts/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="charts/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="charts/js/plugins/morris/raphael.min.js"></script>
    <script src="charts/js/plugins/morris/morris.min.js"></script>
    <script src="charts/js/plugins/morris/morris-data.js"></script>

    <!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
    <script src="charts/js/plugins/flot/jquery.flot.js"></script>
    <script src="charts/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="charts/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="charts/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="charts/js/plugins/flot/flot-data.js"></script>
     
</body>
</html>
