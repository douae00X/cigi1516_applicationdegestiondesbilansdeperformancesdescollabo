<%@page import="DAO.ObjectifsOperations"%>
<%@page import="entities.Objectif"%>
<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
        
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
    </style>
    
        
    <title>New Feedback</title>
    
  </head>
  
      <%!
    List<Objectif> userObjList = new ArrayList<Objectif>() ;
    int i ;
  %>
  <%
    ObjectifsOperations objectigsOperations = new ObjectifsOperations() ;
    //userObjList      = objectigsOperations.getObjListById(collId) ;
  %>
           
  
  <body>
  
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
    
        </div>
      </div>
    </div>
     <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                  <img src="bootstrap-3.3.6-dist/img/user.png" height="42" width="42"/>
                  <font size="3" color="#fff">daddylittlebomb</font>
                </li>
                <li>
                    <a href="encadrant.jsp"><font size="3" color="#fff"><i class="glyphicon glyphicon-home"></i>&nbsp;Home</font></a>
                </li>
            </ul>
        </div>
      
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                    <table class="table table-bordered table-striped">  
                       <%for(i=0 ; i< userObjList.size() ; i++) {%>
                         <tr>
                           <td>  
                             <a href="feedbackSheet.jsp?indice=<%=i%>">
                               Fichier d'objectifs du <%=userObjList.get(i).getDateCre()%>
                             </a> 
                            </td>
                          </tr> 
                       <%}%> 
                    </table> 
                  </div>
                </div>
            </div>
 
        </div>

 </div>
        
</body>
</html>
