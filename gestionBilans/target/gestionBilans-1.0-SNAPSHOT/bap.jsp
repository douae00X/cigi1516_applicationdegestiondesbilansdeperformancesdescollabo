<%@page import="DAO.ObjectifsOperations"%>
<%@page import="entities.Objectif"%>
<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
  <head>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <meta charset="utf-8">
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
        
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
      progress {
        color: #0063a6;
        font-size: .6em;
        line-height: 1.5em;
        text-indent: .5em;
        width: 92.5em;
        height: 3em;
        border: 1px solid #0063a6;
        background: #fff;
     }
    </style>
    
    <script>
   
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope) {   
    $scope.poids = 0;
    $scope.note = 0;

$scope.update = function() {
//$scope.item.size.code = $scope.selectedItem.code
     
$scope.note = parseInt($scope.note11, 10)+parseInt($scope.note21, 10)+
             parseInt($scope.note31, 10)+parseInt($scope.note41, 10)+
             parseInt($scope.note51, 10)+parseInt($scope.note61, 10)+
             parseInt($scope.note71, 10)+parseInt($scope.note81, 10);     
 
    };
}) ;

</script>

    <title>New Feedback</title>
    
  </head>
 
   <%!
    Feedback feed = new Feedback() ;
    Objectif obj = new Objectif() ;
   %>
   <%
    feed= (Feedback) request.getSession().getAttribute("feedbackSheet") ;
    obj = (Objectif) request.getSession().getAttribute("objSheet") ; 
  %> 
  
  <body>
   <form action="manager" method="post">   
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
    
        </div>
      </div>
    </div>
     <div id="wrapper">
      
        
<!---------------------------------Feedback Sheet------------------------------------>
        
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <form class="form-horizontal">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12"> 

  <div class="container-fluid"> 
      <fieldset>
      <legend>Entête</legend>
      <div class="row-fluid">
        <div class="span8">
          <div class="row-fluid">
            <div class="span12 bgcolor">
             
            </div>
          </div> 
          <div class="row-fluid">
            <div>
             <div class="control-group">
               <label class="control-label" for="project">Projet</label>
                <div class="controls">
                    <input type="text" name="projet" value="<%=feed.getProjectName()%>" readonly/>
                </div>
              </div>
            </div><!--/span-->
          </div><!--/row-->  
          <div class="row-fluid">
            <div class="span6 bgcolor">
              <div class="control-group">
                <label class="control-label" for="coll">Collaborateur</label>
                <div class="controls">
                   <input type="text" name="collaborateurName" value="<%=feed.getCollabName()%>" readonly/>  
                </div>
              </div>
            </div><!--/span-->
          <div class="row-fluid">
         <div class="span6 bgcolor">
              <div class="control-group">
                 <label class="control-label" for="dateDebut" >Date début</label>
                <div class="controls">
                    <input type="text" name="dateDebut" value="<%= feed.getDateDebut()%>" readonly>
                </div>
              </div>
            </div><!--/span-->
            <div class="span6 bgcolor">
              <div class="control-group">
                 <label class="control-label" for="dateFin">Date fin</label>
                <div class="controls">
                  <input type="text" name="dateFin" value="<%= feed.getDateFin()%>" readonly>
                </div>
              </div>
            </div><!--/span-->
          </div><!--/row-->
          </fieldset>
          <fieldset>
          <legend>Détails</legend>
             <div id="wrapper">
 
             <div class="container">
   
      <table style="position: absolute; top: 300px; left: 30px" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Thème</th>
            <th>Qualification *</th>
            <th>Remarque</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Productivité</td>
            <td>
               <input type="text" name="prod" value="<%=feed.getProductivité()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r1" value="<%=feed.getRem1()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Qualité/Fiabilité</td>
            <td>
                 <input type="text" name="qty" value="<%= feed.getFiabilité()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r2" value="<%= feed.getRem2()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Technicité</td>
            <td>
                <input type="text" name="tech" value="<%= feed.getTech()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r3" value="<%= feed.getRem3()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Conception</td>
            <td>
                <input type="text" name="conc" value="<%= feed.getConcep()%>" readonly/>   
            </td>
            <td>
                <input type="text" name="r4" value="<%= feed.getRem4()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Avant-vente</td>
            <td>
                <input type="text" name="avte" value="<%= feed.getAvantVente()%>" readonly/>   
            </td>
            <td>
                <input type="text" name="r5" value="<%= feed.getRem5()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Gestion de projet</td>
            <td>
                <input type="text" name="gp" value="<%= feed.getGestionProjet()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r6" value="<%= feed.getRem6()%>" readonly/>
            </td>
          </tr>
	  <tr>
            <td>Gestion de relation client</td>
            <td>
               <input type="text" name="grc" value="<%= feed.getGestionRelation()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r7" value="<%= feed.getRem7()%>" readonly/>
            </td>
          </tr>
          <tr>
            <td>Polyvalence</td>
            <td>
               <input type="text" name="poly" value="<%= feed.getPoly()%>" readonly/>  
            </td>
            <td>
                <input type="text" name="r8" value="<%= feed.getRem8()%>" readonly/>
            </td>
          </tr>
          <tr>
              <td>
                <ul type="disc">  
                <li> <font color="#4097EE">Nombre de thèmes qualifiés</font></li>
                <li> <font color="#4097EE">Total poids obtenu</font></li>
                <li> <font color="#4097EE">Nombre global</font></li>
               </ul>
              </td>
              <td colspan="2">
                  <input type="text" value="<%= feed.getNtq()%>" name="NTQ" readonly/><br>
                  <input type="text" value="<%= feed.getTpo()%>" name="TPO" readonly/><br>
                  <input type="text" value="<%= feed.getNg()%>" name="NG" readonly/>
              </td>    
             
          </tr>
          <tr>
              <td colspan="3">
                   <progress id="progressbar" class="progressbar"  value="20" max="100"></progress><br>
                  <font style="color:#0063a6"><b>Critique&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      A développer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selon attente
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;Démontre des forces</b></font>
                      
              </td>    
             
          </tr>
          <tr>
              <td><b>Remarque générale *:</b></td>
              <td colspan="2">
                  <input  name="rque" style="width:590px;height:200px" value="<%=feed.getRemGlobale()%>" readonly/>
              </td>                 
          </tr>         
        </tbody>
      </table>
    </div>              
                   
          </fieldset>                        
        </div><!--/span-->
        
        <div class="span4">
        
        </div><!--/span-->       
      </div><!--/row-->  
                                  
        </div>
      </div>
   
     </div>
   </div>
 
         
<!------------------------------------------Objectifs Sheet-------------------------------------->        
<div id="wrapper">
    <form action="manager" method="post">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                         

    
    <div class="container-fluid">      
          <fieldset>
  <div class="container">
    <div ng-app="myApp" ng-controller="myCtrl">     
      <table style="position: absolute; top: 800px; left: 30px" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Thème</th>
            <th>Objectifs</th>
            <th>Poids</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Productivité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px;" value="<%= obj.getObjPro()%>" name="obj11" readonly/>
            </td>
            <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsPro()%>" name="poids11" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNotePro()%>" ng-model="note11" ng-change="update()" name="note11" />
            </td>
          </tr>
          <tr>
            <td>Qualité/Fiabilité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjQty()%>" name="obj21" readonly/>
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsQty()%>" name="poids21" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteQty()%>" ng-model="note21" ng-change="update()" name="note21" />
            </td>
           </tr>
          <tr>
            <td>Technicité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjTech()%>" name="obj31" readonly/>
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsTech()%>" name="poids31" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteTech()%>" ng-model="note31" ng-change="update()" name="note31" />
            </td>
          </tr>
          <tr>
            <td>Conception</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjConc()%>" name="obj41" readonly/>
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsConc()%>" name="poids41" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteConc()%>" ng-model="note41" ng-change="update()" name="note41" />
            </td>
          </tr>
          <tr>
            <td>Avant-vente</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjAvv()%>" name="obj51" readonly/> 
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsAvv()%>" name="poids51" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteAvv()%>" ng-model="note51" ng-change="update()" name="note51" />
            </td>
          </tr>
          <tr>
            <td>Gestion de projet</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjGp()%>" name="obj61" readonly/>
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsGp()%>"  name="poids61" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteGp()%>" ng-model="note61" ng-change="update()" name="note61" />
            </td>
          </tr>
	  <tr>
            <td>Gestion de relation client</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjGrc()%>" name="obj71" readonly/>
            </td>
            <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsGrc()%>" name="poids71" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteGrc()%>" ng-model="note71" ng-change="update()" name="note71" />
            </td>
          </tr>
          <tr>
            <td>Polyvalence</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjPoly()%>" name="obj81" readonly/>
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsPoly()%>" name="poids81" readonly/>   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNotePoly()%>" ng-model="note81" ng-change="update()" name="note81" />
            </td>
          </tr>
          <tr>
              <td colspan="3">   
                 
              </td>
              <td>
                  <ul>
                      <li><font color="#4097EE">Note global</font>
                           <p>{{note}}</p>
                      </li>                    
                  </ul>   
               </td> 
           </tr>      
       
        </tbody>
      </table>
    </div>              
                   
          </fieldset>                        
        </div><!--/span-->
        
        <div class="span4">
        </div><!--/span-->       
      </div><!--/row-->  
              <input type="hidden" value="<%= obj.getId()%>" name="objId"/>    
     
        </div>
      </div>
          
    <div class="container-fluid">      
          <fieldset>
        
 
             <div class="container">
    <div> 
      <table style="position: absolute; top: 2000px; left: 30px" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Thème</th>
            <th>Objectifs</th>
            <th>Poids</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Productivité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px;" value="<%= obj.getObjPro()%>" name="obj12" />
            </td>
            <td>
                <input type="text" style="color:green" value="" name="poids12" />   
            </td>
            <td>
                <input type="text" style="color:red" value=""  name="note12" />
            </td>
          </tr>
          <tr>
            <td>Qualité/Fiabilité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjQty()%>" name="obj22" />
            </td>
                 <td>
                <input type="text" style="color:green" value=""  name="poids22" />   
            </td>
            <td>
                <input type="text" style="color:red" value=""  name="note22" />
            </td>
           </tr>
          <tr>
            <td>Technicité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjTech()%>" name="obj32" />
            </td>
                 <td>
                <input type="text" style="color:green" value=""  name="poids32" />   
            </td>
            <td>
                <input type="text" style="color:red" value=""  name="note32" />
            </td>
          </tr>
          <tr>
            <td>Conception</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjConc()%>" name="obj42" />
            </td>
                 <td>
                <input type="text" style="color:green" value="" name="poids42" />   
            </td>
            <td>
                <input type="text" style="color:red" value=""  name="note42" />
            </td>
          </tr>
          <tr>
            <td>Avant-vente</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjAvv()%>" name="obj52" /> 
            </td>
                 <td>
                <input type="text" style="color:green" value="" name="poids52" />   
            </td>
            <td>
                <input type="text" style="color:red" value="" name="note52" />
            </td>
          </tr>
          <tr>
            <td>Gestion de projet</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjGp()%>" name="obj62" />
            </td>
                 <td>
                <input type="text" style="color:green" value=""  name="poids62" />   
            </td>
            <td>
                <input type="text" style="color:red" value="" name="note62" />
            </td>
          </tr>
	  <tr>
            <td>Gestion de relation client</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjGrc()%>" name="obj72" />
            </td>
            <td>
                <input type="text" style="color:green" value=""  name="poids72" />   
            </td>
            <td>
                <input type="text" style="color:red" value="" name="note72" />
            </td>
          </tr>
          <tr>
            <td>Polyvalence</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjPoly()%>" name="obj82" />
            </td>
                 <td>
                <input type="text" style="color:green" value=""  name="poids82" />   
            </td>
            <td>
                <input type="text" style="color:red" value="" name="note82" />
            </td>
          </tr>       
        </tbody>
      </table>
    </div>              
                   
          </fieldset>                        
        </div><!--/span-->
        
        <div class="span4">
         <table border="0"  style="position: absolute; top: 3080px; left: 850px">
          <tr>
             <td></td>
             <td></td>
             <td>
              <input class="btn btn-primary"  type="submit" value="Send New Objectifs" name="newObj"/>
             </td>       
          </tr>    
         </table>    
             <input type="hidden" value="sendBap" name="formlogin"/>    
        </div><!--/span-->   
      </div><!--/row-->  
     </div>
    </form>         
</body>
</html>
