<%@page import="DAO.ObjectifsOperations"%>
<%@page import="entities.Objectif"%>
<%@page import="entities.Feedback"%>
<%@page import="DAO.RetrieveData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Collaborateur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<html lang="en">
  <head>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <meta charset="utf-8">
   
    <link href="bootstrap-3.3.6-dist/css2/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css2/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/simple-sidebar.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css1/bootstrap-responsive.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css3/bootstrap.css" rel="stylesheet">
    
        
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
            
      .lightblue {
       background-color: lightblue;
      }
    
      body {
        min-height: 2000px;
        padding-top: 70px;
      }
      .tableFloatingHeaderOriginal th {
        background-color: #fff;
        border-bottom: 1px solid #DDD;
      }
      progress {
        color: #0063a6;
        font-size: .6em;
        line-height: 1.5em;
        text-indent: .5em;
        width: 92.5em;
        height: 3em;
        border: 1px solid #0063a6;
        background: #fff;
     }
    </style>
    
    <script>
   
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope) {   
    $scope.poids = 0;
    $scope.note = 0;

$scope.update = function() {
//$scope.item.size.code = $scope.selectedItem.code
     
$scope.note = parseInt($scope.note11, 10)+parseInt($scope.note21, 10)+
             parseInt($scope.note31, 10)+parseInt($scope.note41, 10)+
             parseInt($scope.note51, 10)+parseInt($scope.note61, 10)+
             parseInt($scope.note71, 10)+parseInt($scope.note81, 10);     
 
    };
}) ;

</script>

    <title>New Feedback</title>
    
  </head>
 
   <%!
    Feedback feed = new Feedback() ;
    Objectif obj = new Objectif() ;
   %>
   <%
    feed= (Feedback) request.getSession().getAttribute("feedbackSheet") ;
    obj = (Objectif) request.getSession().getAttribute("objSheet") ; 
  %> 
  
  <body>
      
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
    
        </div>
      </div>
    </div>
                   
<!------------------------------------------Objectifs Sheet-------------------------------------->        
<div id="wrapper">
 
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <form action="manager" method="post">
            <div class="container-fluid">
                <div class="row">    
                  <div class="col-lg-12">
                         

    
    <div class="container-fluid">      
          <fieldset>
        
 
             <div class="container">
    <div ng-app="myApp" ng-controller="myCtrl"> 
      <table style="position: absolute; left: 30px" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Thème</th>
            <th>Objectifs</th>
            <th>Poids</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Productivité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px;" value="<%= obj.getObjPro()%>" name="obj11" />
            </td>
            <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsPro()%>" name="poids11" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNotePro()%>" ng-model="note11" ng-change="update()" name="note11" readonly/>
            </td>
          </tr>
          <tr>
            <td>Qualité/Fiabilité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjQty()%>" name="obj21" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsQty()%>" name="poids21" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteQty()%>" ng-model="note21" ng-change="update()" name="note21" readonly/>
            </td>
           </tr>
          <tr>
            <td>Technicité</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjTech()%>" name="obj31" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsTech()%>" name="poids31" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteTech()%>" ng-model="note31" ng-change="update()" name="note31" readonly/>
            </td>
          </tr>
          <tr>
            <td>Conception</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjConc()%>" name="obj41" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsConc()%>" name="poids41" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteConc()%>" ng-model="note41" ng-change="update()" name="note41" readonly/>
            </td>
          </tr>
          <tr>
            <td>Avant-vente</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjAvv()%>" name="obj51" /> 
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsAvv()%>" name="poids51" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteAvv()%>" ng-model="note51" ng-change="update()" name="note51" readonly/>
            </td>
          </tr>
          <tr>
            <td>Gestion de projet</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjGp()%>" name="obj61" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsGp()%>"  name="poids61" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteGp()%>" ng-model="note61" ng-change="update()" name="note61" readonly/>
            </td>
          </tr>
	  <tr>
            <td>Gestion de relation client</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjGrc()%>" name="obj71" />
            </td>
            <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsGrc()%>" name="poids71" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNoteGrc()%>" ng-model="note71" ng-change="update()" name="note71" readonly/>
            </td>
          </tr>
          <tr>
            <td>Polyvalence</td>
            <td>
                <input type="text" style="color:#0063a6;width:400px;height:100px" value="<%= obj.getObjPoly()%>" name="obj81" />
            </td>
                 <td>
                <input type="text" style="color:green" value="<%= obj.getPoidsPoly()%>" name="poids81" />   
            </td>
            <td>
                <input type="text" style="color:red" value="<%= obj.getNotePoly()%>" ng-model="note81" ng-change="update()" name="note81" readonly/>
            </td>
          </tr>
          
        </tbody>
      </table>
    </div>              
                   
          </fieldset>                        
        </div><!--/span-->
        
        <div class="span4">
        </div><!--/span-->       
      </div><!--/row-->  
        
              <input type="hidden" value="sendBip" name="formlogin"/>                
        </div>
      </div>
        
        <div class="span4">
         <table border="0"  style="position: absolute; top: 1090px; left: 850px">
          <tr>
             <td></td>
             <td></td>
             <td>
              <input class="btn btn-primary"  type="submit" value="Send New Objectifs" name="newObj"/>
             </td>       
          </tr>    
         </table>    
               
        </div><!--/span-->   
        </form>
      </div><!--/row-->  
     </div>
    
</body>
</html>
