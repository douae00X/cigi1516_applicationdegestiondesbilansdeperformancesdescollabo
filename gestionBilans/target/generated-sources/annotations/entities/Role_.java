package entities;

import entities.Administrateur;
import entities.Collaborateur;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-01T23:13:12")
@StaticMetamodel(Role.class)
public class Role_ { 

    public static volatile SetAttribute<Role, Collaborateur> users_list;
    public static volatile SingularAttribute<Role, Long> role_id;
    public static volatile SingularAttribute<Role, String> roleName;
    public static volatile SetAttribute<Role, Administrateur> admin_list;

}