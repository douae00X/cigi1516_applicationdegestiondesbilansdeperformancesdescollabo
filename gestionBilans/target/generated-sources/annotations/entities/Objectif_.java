package entities;

import entities.Collaborateur;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-01T23:13:12")
@StaticMetamodel(Objectif.class)
public class Objectif_ { 

    public static volatile SingularAttribute<Objectif, String> poidsConc;
    public static volatile SingularAttribute<Objectif, Collaborateur> coll;
    public static volatile SingularAttribute<Objectif, Float> noteConc;
    public static volatile SingularAttribute<Objectif, String> poidsGrc;
    public static volatile SingularAttribute<Objectif, String> objGp;
    public static volatile SingularAttribute<Objectif, Float> noteQty;
    public static volatile SingularAttribute<Objectif, String> dateCre;
    public static volatile SingularAttribute<Objectif, Float> noteTech;
    public static volatile SingularAttribute<Objectif, Long> id;
    public static volatile SingularAttribute<Objectif, String> objTech;
    public static volatile SingularAttribute<Objectif, String> objGrc;
    public static volatile SingularAttribute<Objectif, Float> notePoly;
    public static volatile SingularAttribute<Objectif, String> poidsPoly;
    public static volatile SingularAttribute<Objectif, String> poidsQty;
    public static volatile SingularAttribute<Objectif, Float> noteAvv;
    public static volatile SingularAttribute<Objectif, String> objPoly;
    public static volatile SingularAttribute<Objectif, String> objAvv;
    public static volatile SingularAttribute<Objectif, String> poidsTech;
    public static volatile SingularAttribute<Objectif, String> poidsPro;
    public static volatile SingularAttribute<Objectif, Float> noteGrc;
    public static volatile SingularAttribute<Objectif, Float> notePro;
    public static volatile SingularAttribute<Objectif, String> poidsAvv;
    public static volatile SingularAttribute<Objectif, String> objPro;
    public static volatile SingularAttribute<Objectif, String> poidsGp;
    public static volatile SingularAttribute<Objectif, String> objConc;
    public static volatile SingularAttribute<Objectif, Float> noteGp;
    public static volatile SingularAttribute<Objectif, String> objQty;

}