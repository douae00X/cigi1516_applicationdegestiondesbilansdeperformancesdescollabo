package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-01T23:13:12")
@StaticMetamodel(Useri.class)
public class Useri_ { 

    public static volatile SingularAttribute<Useri, String> password;
    public static volatile SingularAttribute<Useri, Long> idUtilisateur;
    public static volatile SingularAttribute<Useri, Boolean> enabled;
    public static volatile SingularAttribute<Useri, String> username;

}