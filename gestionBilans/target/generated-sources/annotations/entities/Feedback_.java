package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-01T23:13:12")
@StaticMetamodel(Feedback.class)
public class Feedback_ { 

    public static volatile SingularAttribute<Feedback, String> productivité;
    public static volatile SingularAttribute<Feedback, Long> id;
    public static volatile SingularAttribute<Feedback, String> tech;
    public static volatile SingularAttribute<Feedback, String> rem1;
    public static volatile SingularAttribute<Feedback, String> avantVente;
    public static volatile SingularAttribute<Feedback, String> gestionProjet;
    public static volatile SingularAttribute<Feedback, String> fiabilité;
    public static volatile SingularAttribute<Feedback, String> rem7;
    public static volatile SingularAttribute<Feedback, String> rem6;
    public static volatile SingularAttribute<Feedback, Integer> ntq;
    public static volatile SingularAttribute<Feedback, String> rem8;
    public static volatile SingularAttribute<Feedback, String> rem3;
    public static volatile SingularAttribute<Feedback, String> remGlobale;
    public static volatile SingularAttribute<Feedback, String> rem2;
    public static volatile SingularAttribute<Feedback, String> rem5;
    public static volatile SingularAttribute<Feedback, String> rem4;
    public static volatile SingularAttribute<Feedback, Integer> tpo;
    public static volatile SingularAttribute<Feedback, String> concep;
    public static volatile SingularAttribute<Feedback, String> dateDebut;
    public static volatile SingularAttribute<Feedback, String> poly;
    public static volatile SingularAttribute<Feedback, String> dateFeed;
    public static volatile SingularAttribute<Feedback, Double> ng;
    public static volatile SingularAttribute<Feedback, String> collabName;
    public static volatile SingularAttribute<Feedback, String> dateFin;
    public static volatile SingularAttribute<Feedback, String> gestionRelation;
    public static volatile SingularAttribute<Feedback, String> projectName;

}