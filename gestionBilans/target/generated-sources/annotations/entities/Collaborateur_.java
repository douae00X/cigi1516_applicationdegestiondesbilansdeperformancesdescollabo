package entities;

import entities.ManagerRH;
import entities.Project;
import entities.Role;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-01T23:13:12")
@StaticMetamodel(Collaborateur.class)
public class Collaborateur_ extends Utilisateur_ {

    public static volatile SingularAttribute<Collaborateur, ManagerRH> mrh;
    public static volatile SingularAttribute<Collaborateur, Role> role;
    public static volatile SingularAttribute<Collaborateur, Project> project;
    public static volatile SingularAttribute<Collaborateur, String> dateEntree;

}